#!/bin/bash

#The usage function displaying the information of the command
function usage ()
{
	echo -e "CUT \e[92mversion 1.0\e[39m"
    echo ""
    echo -e "\e[33mDescription : \e[39m"
    echo -e "  Generate unit test file from an object file (.o)"
    echo ""
    echo -e "\e[33mOptions : \e[39m"
    echo -e "  \e[92m-h, --help\e[39m   display this help message"
    echo -e "  \e[92m-i, --init\e[39m   create the tests directory"
    echo -e "  \e[92m--make-tests\e[39m build the tests files"
    echo -e "  \e[92m--run-tests\e[39m  run the tests"
    echo ""
    echo -e "\e[33mAuthors : \e[39m"
    echo -e "  \e[92m2019\e[39m - \e[91mI\e[39mSEN \e[91mT\e[39moulon \e[91mO\e[39mpen \e[91mO\e[39mrganization"
    echo -e "         |- Luca Mayer Dalverny"
    exit 0
}

#The init function creating the tests directory
function init ()
{
    if [ -d "tests" ]
    then
        echo -e "\e[31mError\e[39m : the \e[33mtests/\e[39m directory already exists"
        exit 1
    else
        mkdir "tests"
        
        if ! [ -d "bin" ]
        then
			mkdir "bin"
		fi
        
	echo -e "#include<stdio.h>\n#include<stdlib.h>" >> tests/.includes
	echo -e "\tputs(\"Start running tests ...\");" >> tests/.calls
	reload_main
	echo -e "CFLAGS = -Wall -g -std=gnu99\n" >> tests/makefile
	echo -e "OBJECT= \n" >> tests/makefile
	echo -e "tests.exe: tests.o \$(OBJECT)\n\tgcc \$(CFLAGS) \$^ ../libs/cut/src/assertions.a -o ../bin/tests.exe\n" >> tests/makefile
	echo -e "tests.o: tests.c\n\tgcc \$(CFLAGS) -c tests.c\n" >> tests/makefile
    fi
}

function reload_main ()
{
	rm -f tests/tests.c
	cp tests/.includes tests/tests.c
	echo -e "\nint main(void)\n{\n\t" >> tests/tests.c
	cat tests/.calls >> tests/tests.c
	echo -e "\ttests_status();\n\treturn 0;\n}" >> tests/tests.c
}

#The make test function compiling the tests files
function make_tests ()
{
    if ! [ -d "tests" ]
    then
        echo -e "\e[31mError\e[39m : the \e[33mtests/\e[39m directory is missing"
        echo -e "Perhaps you should try running \e[33mCUT\e[39m with the \e[92m--init\e[39m option"
        exit 1
    else
        cd tests
        rm -f tests.o
        make tests.exe
    fi
}

#The run test function running the tests
function run_tests ()
{
    if ! [ -d "tests" ]
    then
        echo -e "\e[31mError\e[39m : the \e[33mtests/\e[39m directory is missing"
        echo -e "Perhaps you should try running \e[33mCUT\e[39m with the \e[92m--init\e[39m option"
        exit 1
    else
	bin/tests.exe
    fi
}

function unit_test ()
{
    if ! [ -d "tests" ]
    then
        echo -e "\e[31mError\e[39m : the \e[33mtests/\e[39m directory is missing"
        echo -e "Perhaps you should try running \e[33mCUT\e[39m with the \e[92m--init\e[39m option"
        exit 1
    else
        if [ -f "$1" ]
        then
        
        #Going inside the tests/ folder
        cd "tests"
        
	    #Getting the directory and the file name from the path given
	    DIR=`dirname "$1"`
	    FILE=`basename "$1"`
	    
	    #Creating an array from the directory
	    ARRAY_DIR=(${DIR//// })

	    #Creating the name of the test file and the associated object file
	    NAME=`echo "$FILE" | cut -d'.' -f1`
	    TEST_FILE="test_${NAME}.c"
	    
	    #Creating the directory to the libs/cut/src/assertions.h
	    LIBS_DIR="../libs/cut/src/assertions.h"
	    
	    #Creating the directory to the header of the object file
	    HEADER_DIR="../${DIR}/${NAME}.h"
	    
	    #If the header file linked to the obect file is missing
	    if ! [ -f "${HEADER_DIR}" ]
	    then
	    #end of the programm
	    echo -e "\e[31mError\e[39m : the \e[33m${HEADER_DIR}\e[39m file is missing"
	    exit 1
	    fi
	    
	    #If the test unit file already exists
	    if [ -f "${DIR}/${TEST_FILE}" ]
	    then
	    #end of the programm
	    echo -e "\e[31mError\e[39m : the \e[33m${TEST_FILE}\e[39m test file already exists"
	    exit 1
	    else
	    
	    #Filling a buffer file with the data of the object file
	    nm "../$1" | grep "T" | cut -d' ' -f3 > .buffer
	    
	    #Creating the buffer directory
	    BUF_DIR=".buffer"
	    
	    #Adding a line in the .includes file in order to build the test file
	    echo -e "#include\"${DIR}/${TEST_FILE}\"" >> .includes
	    
	    #Adding a line in the .calls file in order to run the test file
	    echo -e "\ttest_${NAME}();" >> .calls
	    
	    OBJECT_DIR="../$1"
	    
	    #Adding the object file to the makefile
	    sed "s|OBJECT=|OBJECT= $OBJECT_DIR|" makefile > .makefile
	    cat .makefile > makefile
	    
	    #Reloading the tests main file
	    cd "../"
	    reload_main
	    cd "tests"

	    for i in range $(seq 1 ${#ARRAY_DIR[@]});
	    do
		#To avoid the empty string at the end of the array
		if ! [ "${ARRAY_DIR[i]}" = "" ]
		then
		    #If the folder does not already exist
		    if ! [ -d "${ARRAY_DIR[i]}" ]
		    then
			#We create it
			mkdir ${ARRAY_DIR[i]}
		    fi
		    #We move inside the folder
		    cd "${ARRAY_DIR[i]}"
		    
		    #We modify the LIBS_DIR and the BUF_DIR variables
		    LIBS_DIR="../${LIBS_DIR}"
		    BUF_DIR="../${BUF_DIR}"
		    HEADER_DIR="../${HEADER_DIR}"
		fi
	    done

		#Creation of the files containing the names of the test functions
	    sed 's/$/();/' "$BUF_DIR" > .cbuff
	    sed 's/^/\ttest_/' .cbuff > .fcalls
	    sed 's/$/(void);/' "$BUF_DIR" > .tbuff
	    sed 's/^/void test_/' .tbuff > .ftypes
	    echo -e "\n" >> .ftypes
	    sed 's/;/{\n\t\/\/TODO (AUTO GENERATED COMMENT)\n\tassertion_true(false);\n}\n/' .ftypes >> .ftypes

		#Creation of the test file
	    echo -e "#include <stdio.h>\n#include <stdlib.h>\n\n#include \"${LIBS_DIR}\"\n#include \"${HEADER_DIR}\"" >> "$TEST_FILE"
	    cat .ftypes >> "$TEST_FILE"
	    echo -e "\n//The main function of this test file\nvoid test_${NAME}(void);\n" >> "$TEST_FILE"
	    echo -e "void test_${NAME}(void)\n{" >> "$TEST_FILE"
	    echo -e "\tputs(\"\");\n\tputs(\"running \e[33mtest_${NAME}\e[39m test file\");" >> "$TEST_FILE"
	    cat .fcalls >> "$TEST_FILE"
	    echo -e "\tputs(\"\");\n}\n" >> "$TEST_FILE"
	    
	    #Removing the buffer files
	    rm .cbuff
	    rm .tbuff
	    rm .fcalls
	    rm .ftypes
	    
	    fi

        else
            echo -e "\e[31mError\e[39m : the input file does not exist"
            exit 1
        fi
    fi
}

#If no arguments are given, we call the usage function
if [ "$#" -eq 0 ]
then
    usage
fi

#Option handling loop
while :; do
    case $1 in
    #If the option given is -h or --help, the usage function is called
    -h|--help)
        usage
        exit 0
        ;;
    #If the option given is -i or --init, the init function is called
    -i|--init)
        init
        exit 0
        ;;
    #If the option given is --make-tests, the make_tests function is called
    --make-tests)
	make_tests
	exit 0
	;;
    #If the option given is --run-tests, the run_tests funciton is called
    --run-tests)
	run_tests
	exit 0
	;;
    --)
        shift
        break
        ;;
    *)
        break;
    esac
done

if [ "$#" -eq 1 ]
then
    unit_test "$1"
else
    echo -e "\e[31mError\e[39m : too many arguments given"
    exit 1
fi

exit 0
