#include <stdio.h>
#include <stdlib.h>

#include "assertions.h"

static int test_failure(boolean new_failure)
{
	static int failed_test = 0;

	if(new_failure)
	{
		failed_test+=1;
		printf("\x1b[31mF\x1b[0m.");
	}

	return failed_test;
}

static int test_success(boolean new_success)
{
	static int successful_test = 0;

	if(new_success)
	{
		successful_test+=1;
		printf("\x1b[32mS\x1b[0m.");
	}

	return successful_test;
}

void tests_status(void)
{
	printf("\n\x1b[33mCUT\x1b[0m tests summary :\n");
	printf("-failures : %d\n",test_failure(false));
	printf("-success : %d\n",test_success(false));
	printf("-total : %d\n",test_failure(false)+test_success(false));
}

void assertion_true(boolean assertion)
{
	if(assertion)
	{
		test_success(true);
	}
	else
	{
		test_failure(true);
	}
}

void assertion_false(boolean assertion)
{
	if(!assertion)
	{
		test_success(true);
	}
	else
	{
		test_failure(true);
	}
}

void assertion_null(void* assertion)
{
	if(assertion == NULL)
	{
		test_success(true);
	}
	else
	{
		test_failure(true);
	}
}

void assertion_not_null(void* assertion)
{
	if(assertion != NULL)
	{
		test_success(true);
	}
	else
	{
		test_failure(true);
	}
}
