/**
 * @title assertions.h
 * @date 5 avr. 2019
 * @docauthor luca
 * @make
 */

#ifndef ASSERTIONS_H_
#define ASSERTIONS_H_

#include "boolean.h"

void tests_status(void);

void assertion_true(boolean assertion);

void assertion_false(boolean assertion);

void assertion_null(void* assertion);

void assertion_not_null(void* assertion);

#endif /* ASSERTIONS_H_ */
