/**
 * @title boolean.h
 * @date 26 mars 2019
 * @docauthor Luca Mayer-Dalverny
 * @make
 */

#ifndef BOOLEAN_H_
#define BOOLEAN_H_

typedef enum boolean{false = 0, true = 1}boolean;

#endif /* BOOLEAN_H_ */
