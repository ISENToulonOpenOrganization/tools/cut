CFLAGS = -Wall -g -std=gnu99

ASSERTIONS_C = src/assertions.c
ASSERTIONS_H = src/assertions.h
BOOLEAN_H = src/boolean.h

ASSERTIONS_A = src/assertions.a

release: assertions.a
	tar -cvf cut.tar $(ASSERTIONS_H) $(ASSERTIONS_A) $(BOOLEAN_H) cut.sh
	
assertions.a: assertions.o
	ar -rv $(ASSERTIONS_A) assertions.o
	
assertions.o: $(ASSERTIONS_C) $(ASSERTIONS_H) $(BOOLEAN_H)
	gcc $(CFLAGS) -c $(ASSERTIONS_C)
	
clean:
	rm -f *.o
	rm -f *.exe