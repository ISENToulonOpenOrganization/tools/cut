# CUT (C Unit Test)

CUT est un outil permettant de générer des fichiers de test pour un projet codé en C. Il fournit également un jeu de fonctions pour la validation des tests et l'affichage des résultats

## Sommaire

1. [Informations utiles](#info)
    1. [Classes conseillées](#classes)
    2. [Concepts requis](#concepts)
2. [À propos](#apropos)
    1. [Comment ça marche ?](#comment)
    2. [Pour qui est-ce que cela a été conçu ?](#qui)
3. [Utilisation dans vos projets](#utilisation)
    1. [Pré-requis](#prerequis)
    2. [Installation](#installation)
    3. [Utilisation](#utilisation)
4. [Crédits](#credits)
5. [Alternatives](#alternatives)
6. [Mainteneurs](#mainteneurs)

## Informations utiles <a name=info></a>

### Classes conseillées <a name=classes></a>

+ Année minimum : N1
+ Domaine Professionnel : Tous
+ Cycle fait : Tous

### Concepts requis à la compréhension du code <a name=concepts></a>

+ Bash
+ Commandes UNIX
+ Le langage C

## À propos de CUT <a name=apropos></a>

### Comment ça marche ? <a name=comment></a>

Le code est un gros fichier bash pour la génération des fichiers et de quelques fichiers c pour les fonctions de validation des tests

### Pour qui est-ce que cela a été conçu ? <a name=qui></a>

CUT a été conçu pour les personne codant en c et voulant faire des tests unitaire pour leurs fonctions sans avoir à tout écrire.

## Utilisation de CUT dans vos projets <a name=utilisation></a>

### Pré-requis <a name=prerequis></a>

Il est recommandé d'avoir généré l'arborescence de son programme avec [ProGen](https://framagit.org/ISENToulonOpenOrganization/tools/progen).

### Installation <a name=installation></a>

+ Téléchargez la dernière version de CUT [ici](https://framagit.org/ISENToulonOpenOrganization/tools/cut/tags)
+ Créez un dossier "libs" à la racine de votre projet (inutile si vous avez utilisé [ProGen](https://framagit.org/ISENToulonOpenOrganization/tools/progen))
+ Dézippez l'archive cut.tar dans le dossier "libs"

### Utilisation <a name=utilisation></a>

#### Initialisation

Tout d'abord, initialisez le répertoire de test :

```bash
libs/cut/cut.sh --init
```

#### Génération d'un fichier de test

Pour générer le fichier de tests associé à un fichier c, vous devez générer un fichier objet à l'aide de la commande gcc, puis, utiliser cut.sh :

```bash
libs/cut/cut.sh src/path/file.o
```

Le fichier généré se nomme "test_file.c" et se trouve dans "tests/src/path/". Vous pouvez remplir les fonctions de test présentes dans ce fichier et utiliser les fonctions présente dans "libs/cut/src/assertions.h" pour la validation des tests.

#### Compilation et execution des fichiers de tests

Pour compiler les fichiers de test :

```bash
libs/cut/cut.sh --make-tests
```

Pour lancer les tests :

```bash
libs/cut/cut.sh --run-tests
```

Cette dernière commande va vous afficher quel fichier de test est en train d'être executé avec les tests qui réussissent et ceux qui échouent. Un décompte du nombre d'échecs et de succès est donné à la fin de l'execution de tout les tests.

## Crédits <a name=credits></a>

+ [ProGen](https://framagit.org/ISENToulonOpenOrganization/tools/progen)

## Alternatives <a name=alternatives></a>

+ [CUnit](http://cunit.sourceforge.net/)

## Mainteneurs <a name=mainteneurs></a>

+ [@LucaMayerDalverny](https://framagit.org/LucaMayerDalverny)
